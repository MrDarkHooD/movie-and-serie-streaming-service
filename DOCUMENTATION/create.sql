CREATE TABLE shows (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name TEXT NOT NULL,
    type TINYINT(1),
    insert_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_update TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    description TEXT,
    genre TEXT,
    year CHAR(9),
    released CHAR(11),
    country TEXT,
    rated CHAR(10),
    director TEXT,
    writer TEXT,
    actors TEXT,
	awards TINYTEXT,
	boxoffice BIGINT,
	production TEXT,
	website varchar(255),
	imdb_votes BIGINT,
	imdb_rating TINYINT,
	RottenTomatoes_rating TINYINT,
	Metacritic_rating TINYINT,
    imdbID VARCHAR(10) NOT NULL,
	imdbTrailerID VARCHAR(15),
	title_image VARCHAR(4),
	cover VARCHAR(4),
	medium_poster VARCHAR(4),
	big_poster VARCHAR(4),
	ad_poster VARCHAR(4)
	visibility TINYINT
    PRIMARY KEY (id)
) ENGINE=INNODB;

COMMENT ON COLUMN shows.visibility IS '0 = public, 1=coming, 2 = hidden';
COMMENT ON COLUMN shows.type IS '0 = serie, 1=movie';

CREATE TABLE episodes (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name TEXT NOT NULL,
    insert_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_update TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    description TEXT,
	episode_number MEDIUMINT(9),
	genre TINYTEXT,
    production_code TEXT,
    rated CHAR(10),
    director TEXT,
    writer TEXT,
    actors TEXT,
    airdate TEXT,
	year CHAR(9),
    released CHAR(11),
    country TEXT,
	production TEXT,
	awards TINYTEXT,
	website varchar(255),
	imdb_votes BIGINT,
	imdb_rating TINYINT,
	RottenTomatoes_rating TINYINT,
	Metacritic_rating TINYINT,
	season_id BIGINT NOT NULL,
	imdbID VARCHAR(10) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE files (
    id BIGINT NOT NULL AUTO_INCREMENT,
    insert_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_update TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    file TEXT NOT NULL,
	extension VARCHAR(4),
	quality VARCHAR(4),
	mimetype TINYTEXT,
	duration MEDIUMINT(9),
	credit_start_time MEDIUMINT(9),
	imdbID VARCHAR(10) NOT NULL
    PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE subtitles (
    id BIGINT NOT NULL AUTO_INCREMENT,
    insert_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_update TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    subtitle BLOB NOT NULL,
	file_id BIGINT NOT NULL,
	imdbID VARCHAR(10) NOT NULL,
	FOREIGN KEY (file_id)
        REFERENCES files(id)
		ON DELETE CASCADE,
    FOREIGN KEY (lang_id)
        REFERENCES languages(id)
    PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TRIGGER movie_file_cleaner
    AFTER DELETE
    ON shows FOR EACH ROW
    DELETE FROM
    files WHERE files.movie_id = OLD.id;

CREATE TRIGGER episode_file_cleaner
    AFTER DELETE
    ON episodes FOR EACH ROW
    DELETE FROM
    files WHERE files.id = OLD.file_id;

-- Here starts site settings

CREATE TABLE categories (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name TINYTEXT NOT NULL,
    image TEXT,
    PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE languages (
    id INT NOT NULL AUTO_INCREMENT,
    name BIGINT NOT NULL,
	iso_639_1 char(3),
    iso_639_2 char(2),
	locale char(5),
	currency char(3),
	currency_symbol VARCHAR(5),
	flag_identifier  TEXT NOT NULL,
    PRIMARY KEY (id)
) ENGINE=INNODB;

COMMENT ON COLUMN languages.iso_639_1 IS 'long';
COMMENT ON COLUMN languages.iso_639_2 IS 'short';

-- User tables
CREATE TABLE users (
    id BIGINT NOT NULL AUTO_INCREMENT,
    nick TINYTEXT NOT NULL,
    password  CHAR(60) NOT NULL,
    login_id TEXT NOT NULL,
    admin TINYINT(1) DEFAULT 0 NOT NULL,
    PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE user_settings (
    id BIGINT NOT NULL AUTO_INCREMENT,
    user_id BIGINT NOT NULL,
    email VARCHAR(320),
    image_titles TINYINT(1) DEFAULT 1 NOT NULL,
    admin TINYINT(1) DEFAULT 0 NOT NULL,
	lang_id INT DEFAULT 1 NOT NULL,
	FOREIGN KEY (lang_id)
        REFERENCES languages(id),
    PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE login_history (
    id BIGINT NOT NULL AUTO_INCREMENT,
    userid BIGINT NOT NULL,
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ip BINARY(16) NOT NULL,
    FOREIGN KEY (userid)
        REFERENCES users(id),
    PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE user_own_list (
    id BIGINT NOT NULL AUTO_INCREMENT,
    userid BIGINT NOT NULL,
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    show_id BINARY(16) NOT NULL,
    FOREIGN KEY (show_id)
        REFERENCES shows(id),
    PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE user_last_watched_episode (
    id BIGINT NOT NULL AUTO_INCREMENT,
    userid BIGINT NOT NULL,
    time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    episode_id BINARY(16) NOT NULL,
    FOREIGN KEY (episode_id)
        REFERENCES episodes(id),
    PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE user_watch_history (
    id BIGINT NOT NULL AUTO_INCREMENT,
    userid BIGINT NOT NULL,
    imdbID VARCHAR(10) NOT NULL,
    watched_time MEDIUMINT,
    PRIMARY KEY (id)
) ENGINE=INNODB;