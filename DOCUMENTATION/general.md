# General

## Supported stuff
* Video qualitys:
  * 360p
  * 720p
  * 1080p
  * 4K
  * 8K
* Languages:
  * en -> English
  * fi -> Finnish
  * sv -> Svedish
  * da -> Danish
  * no -> Norwegian
  * fr -> French
  * it -> Italian
  * es -> Spanish
  * ro -> Romanian
  * pl -> Portuguese