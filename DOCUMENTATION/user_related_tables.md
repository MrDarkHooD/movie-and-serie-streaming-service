## users
* id (key)
* nick
* password
* login_id
  * Used to prevent multiple logins
* admin

## user_settings
* id
* user_id
* email
* image_titles
  * Will be removed
* admin
* lang_id
  * Relation to languages table (id)

## 