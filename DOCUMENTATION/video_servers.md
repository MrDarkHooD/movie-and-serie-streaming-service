# Video server

## Rules
Naming: video_{num} (3 digit)  
Domain: v{num}.domain.ltd (3 digit)  
Server label: v{num} (3 digit)  
Web server path: /www/  

* Basic www folder structure
  * In root is index.html containing "Sitename video server v{num}" (3 digits)
  * In root is disk.json containing data about disk space
  * Every show has own directory named with imdb id
  * Seasons have own folder inside show folder
  * Season images are in season folder

* Video- & related files
  * Video name can be anything
  * Different qualitys are named as: {original_filename}_{quality}.{extension}
  * Preview image must be named as: {original_filename}.{extension}
  * Subtitles are saved in database, but they can be saved as files named as: {original_filename}_{lang_code}.vtt
	
## Uploading video

### Required files
* Movie/serie itself
* Medium poster (This goes to img folder on main server)

### Optional files
* Advertising image (Required to make ad)
* Subtitles (vtt)

### Required data
* Video duration
* Start time of credits
* imdbID
* Description

## Creating new show

### Files
* basic_poster.ext (600px*900px)
* medium_poster.ext (1920px*1080px)
* big_image (1920px*1080px)
* ad_poster.ext (1920px*1080px)
* title.ext (1920px*1080px)

## Creating new season

### Files
Show needs poster image.

## Automated tasks
	
### /www/disk.json updater
**/opt/disk.py**
```python
import json
import psutil

def sizeof_fmt(num):
    for unit in ['B','KB','MB','GB','TB','PB','EB','ZB']:
        if abs(num) < 1024.0:
            return "%3.1f %s" % (num, unit)
        num /= 1024.0
    return "%.1f %s" % (num, 'YB')

hdd = psutil.disk_usage('/')

output = {}

for name in ['total', 'free', 'used']:
    output[name] = {}
    output[name]["bytes"] = getattr(hdd, name)
    output[name]["human"] = sizeof_fmt(getattr(hdd, name))
    output[name]["percent"] = round(getattr(hdd, name)/hdd.total*100, 2)

print (json.dumps(output))
```

**/opt/disk.sh**
```bash
#!/bin/bash
python3 /opt/disk.py > /www/disk.json
```
**/etc/systemd/system/www_disk_json_update.service**
```ini
[Unit]
Description=Update /www/disk.json

[Service]
Type=oneshot
User=sysop
ExecStart=/opt/disk.sh
RemainAfterExit=false
StandardOutput=journal
```

**/etc/systemd/system/www_disk_json_update.timer**
```ini
[Unit]
Description=Update /www/disk.json

[Timer]
OnCalendar=*:0/1
Persistent=true

[Install]
WantedBy=timers.target
```
