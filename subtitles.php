<?php
include("include/topinclude.php");

if(!isset($_GET["id"]))
  die("Show id not given");

header("Content-Type: text/vtt");
$id = $_GET["id"];

if(isset($_GET["lang"])) {
  $lang = $_GET["lang"];
}else {
  $lang = "en";
}

$query = $db->prepare("SELECT subtitle FROM subtitles WHERE file_id = ? AND lang = ?");

if($query->execute(array($id, $lang))) {
    echo $query->fetchColumn();
}else {
    die("Subtitles not found");
}